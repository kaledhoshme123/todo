-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 31 مايو 2021 الساعة 14:24
-- إصدار الخادم: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `todo`
--

-- --------------------------------------------------------

--
-- بنية الجدول `todo_content`
--

CREATE TABLE `todo_content` (
  `user_id` int(11) NOT NULL,
  `content` longtext NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- إرجاع أو استيراد بيانات الجدول `todo_content`
--

INSERT INTO `todo_content` (`user_id`, `content`, `id`) VALUES
(1, 'dfdfgdfg', 1),
(2, 'dfdfgdfg', 3),
(2, 'df', 4),
(2, 'koko koko yes', 5),
(2, 'koko koko yes 12312321', 6);

-- --------------------------------------------------------

--
-- بنية الجدول `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- إرجاع أو استيراد بيانات الجدول `user`
--

INSERT INTO `user` (`id`, `email`, `name`) VALUES
(1, 'kaledhoshe123@gmail.com', 'khaled'),
(2, 'df1@hotmail.com', 'df'),
(3, 'df2@hotmail.com', 'df'),
(4, 'df3@hotmail.com', 'df'),
(5, 'df4@hotmail.com', 'df'),
(38, 'Mathias_Muller@gmail.com', 'Griffin'),
(39, 'Keara68@hotmail.com', 'Maximillian'),
(40, 'Adrien68@gmail.com', 'Lorine'),
(41, 'Mellie_Roob59@hotmail.com', 'Carmela'),
(42, 'Dwight48@gmail.com', 'Evie'),
(43, 'Hiram_Berge54@hotmail.com', 'Nikolas'),
(44, 'Ezra.Ullrich33@gmail.com', 'Osvaldo'),
(45, 'Luis.Collins84@hotmail.com', 'Annamae'),
(46, 'Arjun_Sporer4@yahoo.com', 'Torrance'),
(47, 'Pearline.Oberbrunner@yahoo.com', 'Mandy'),
(48, 'Dawn_Prohaska42@yahoo.com', 'Jennyfer'),
(49, 'Rey_Pagac83@hotmail.com', 'Nola'),
(50, 'Merle_Mayert4@gmail.com', 'Angela'),
(51, 'Cydney22@gmail.com', 'Natalie'),
(52, 'Robin.Harvey85@yahoo.com', 'Alford'),
(53, 'Ivy11@gmail.com', 'Thalia'),
(54, 'Carissa.Swift@yahoo.com', 'Bryana'),
(55, 'Jermaine.Bogisich75@yahoo.com', 'Blanche'),
(56, 'Afton51@yahoo.com', 'Neal'),
(57, 'Hobart.Kuhic@hotmail.com', 'Raegan'),
(58, 'Madelyn_Wolff@hotmail.com', 'Davon'),
(59, 'Isabel_Kutch75@hotmail.com', 'Karli'),
(60, 'Carmella.Conn82@yahoo.com', 'Coleman'),
(61, 'Mariano_Marvin@yahoo.com', 'Verdie'),
(62, 'Lennie1@gmail.com', 'Willa'),
(63, 'Mariane.Lubowitz@yahoo.com', 'Rhea'),
(64, 'Shayna70@gmail.com', 'Camren'),
(65, 'Rowena91@gmail.com', 'Cordell'),
(66, 'Amari.Ankunding69@hotmail.com', 'Ardith'),
(67, 'Elnora.OConner@yahoo.com', 'Eulah'),
(68, 'Sunny_Lowe59@yahoo.com', 'Diego'),
(69, 'Kelsi_Wisozk43@yahoo.com', 'Jennings'),
(70, 'Amelie.Haley63@gmail.com', 'Cleve');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `todo_content`
--
ALTER TABLE `todo_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `todo_content`
--
ALTER TABLE `todo_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- قيود الجداول المحفوظة
--

--
-- القيود للجدول `todo_content`
--
ALTER TABLE `todo_content`
  ADD CONSTRAINT `todo_content_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
